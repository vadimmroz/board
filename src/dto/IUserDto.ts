export interface IUserDto {
  name: string
  color: string
  date: string
  role: string
}
