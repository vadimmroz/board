import classes from './preloder.module.scss'
import classNames from 'classnames'
const Preloader = ({ className }: { className?: string }) => {
  return (
    <div className={classNames(classes.preloader, className)}>
      <figure />
    </div>
  )
}

export default Preloader
