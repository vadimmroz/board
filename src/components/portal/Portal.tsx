'use client'

import { createPortal } from 'react-dom'
import { PropsWithChildren, useEffect, useState } from 'react'

export default function Modal({ children }: PropsWithChildren) {
  const [mounted, setMounted] = useState(false)

  useEffect(() => setMounted(true), [])

  return mounted ? createPortal(<>{children}</>, document.body) : null
}
