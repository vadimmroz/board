import React from 'react'
import classNames from 'classnames'
import classes from '@/plugins/tasks/list/card/progress/progress.module.scss'

const ProgressBar = ({ progress }: { progress: number }) => {
  return (
    <figure>
      <figure
        style={{ width: progress + '%' }}
        className={classNames(
          progress < 30 && classes.red,
          progress >= 30 && classes.yellow,
          progress >= 70 && classes.green
        )}
      ></figure>
    </figure>
  )
}

export default ProgressBar
