'use client'
import classes from './profile.module.scss'
import Image from 'next/image'
import Button from '@/components/UIKit/button'
import { useRouter } from 'next/navigation'
import axios from 'axios'
import { url } from '@/utils/utils'
import { useEffect, useState } from 'react'
import { User } from './utils'
import Modal from '@/components/portal'
import EditImg from './components/editImg'

const getProfile = async () => {
  const token = await localStorage.getItem('token')
  return axios.get(`${url}/auth/profile`, {
    headers: { Authorization: `Bearer ${token || ''}` },
  })
}
const Profile = () => {
  const [data, setData] = useState<User | null>(null)
  const [editImg, setEditImg] = useState(false)
  const [img, setImg] = useState('')
  const router = useRouter()

  useEffect(() => {
    getProfile().then((res) => {
      setData(res.data)
    })
  }, [])

  useEffect(() => {
    if (data?.id) {
      axios.get(`${url}/auth/profile/${data?.id}/img`).then((res) => {
        setImg(res.data)
      })
    }
  }, [data, editImg])

  const handleLogout = () => {
    localStorage.removeItem('token')
    router.push('/')
  }
  const handleEditImg = () => {
    setEditImg(true)
  }

  return (
    <div className={classes.profile}>
      <div className={classes.user}>
        <div onClick={handleEditImg}>
          <Image
            src={img[0] !== 'd' ? '/img/testUser.png' : img}
            width={150}
            height={150}
            alt={'person'}
          />
        </div>
        <div className={classes.content}>
          <h2>{data?.name}</h2>
          <p>Role: {data?.role}</p>

          <div className={classes.btns}>
            <Button color={'default'} size={'small'}>
              Settings
            </Button>
            <Button onClick={handleLogout} color={'danger'} size={'small'}>
              Logout
            </Button>
          </div>
        </div>
      </div>
      {editImg && (
        <Modal>
          <EditImg setEditImg={setEditImg} id={data?.id} />
        </Modal>
      )}
    </div>
  )
}

export default Profile
