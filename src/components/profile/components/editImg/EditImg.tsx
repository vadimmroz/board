'use client'
import classes from './editImg.module.scss'
import { Dispatch, useState } from 'react'
import { Button, Input, Typography } from 'antd'
import axios from 'axios'
import { url } from '@/utils/utils'

type Props = {
  setEditImg: Dispatch<boolean>
  id: string | undefined
}
const EditImg = ({ setEditImg, id }: Props) => {
  const [img, setImg] = useState<FileList | null>(null)
  const close = () => {
    setEditImg(false)
  }
  const handleChange = () => {
    const form = new FormData()
    form.append('file', img?.[0] || '')
    axios.post(`${url}/auth/profile/${id}/img`, form).then((res) => {
      console.log(res)
      setEditImg(false)
    })
  }
  return (
    <div className={classes.editImg}>
      <form>
        <button className={classes.close} onClick={close}>
          &times;
        </button>
        <Typography>Change Image</Typography>
        <Input type="file" onChange={(e) => setImg(e.currentTarget.files)} />
        <Button onClick={handleChange}>Change</Button>
      </form>
    </div>
  )
}

export default EditImg
