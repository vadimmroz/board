export interface User {
  id?: string
  name: string
  password: string
  date: Date
  color: string
  role: string
  theme: boolean
  img: string
}
