import classes from './dashboardDND.module.scss'
import { ReactNode } from 'react'

const DashboardDnd = ({ children }: { children: ReactNode }) => {
  return <section className={classes.dashboardDND}>{children}</section>
}

export default DashboardDnd
