import classes from './pageHeader.module.scss'
import { ReactNode } from 'react'
const PageHeader = ({ children }: { children: ReactNode }) => {
  return <div className={classes.pageHeader}>{children}</div>
}

export default PageHeader
