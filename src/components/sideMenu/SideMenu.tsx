import classes from './sideMenu.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import Menu from './components/menu'
import { apps, pages } from './utils'
import Button from '@/components/sideMenu/components/button'
import List from '@/components/sideMenu/components/List'

const SideMenu = () => {
  return (
    <div className={classes.sideMenu}>
      <Link href="/" style={{ marginBottom: 50 }}>
        <Image
          src="/img/logoFull.svg"
          alt="logo full"
          width={148}
          height={32}
        />
      </Link>
      <Menu name="Pages">
        {pages.map((e, i) => {
          if (e.type === 'button') {
            return (
              <Button
                name={e.name}
                key={Math.random()}
                href={e.href}
                src={e.img}
              />
            )
          } else {
            return (
              <List
                list={e.list}
                name={e.name}
                key={Math.random()}
                src={e.img}
              />
            )
          }
        })}
      </Menu>
      <Menu name="Apps">
        {apps.map((e, i) => {
          if (e.type === 'button') {
            return (
              <Button
                key={Math.random()}
                name={e.name}
                href={e.href}
                src={e.img}
              />
            )
          } else {
            return (
              <List
                list={e.list}
                key={Math.random()}
                name={e.name}
                src={e.img}
              />
            )
          }
        })}
      </Menu>
      <Menu name="Settings">
        <Button
          name="My Profile"
          href="/profile"
          src="/img/sideMenu/setting-2.svg"
        />
      </Menu>
    </div>
  )
}

export default SideMenu
