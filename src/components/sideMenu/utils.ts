export const pages = [
  {
    type: 'button',
    name: 'Dashboard',
    href: '/dashboard',
    img: '/img/sideMenu/graph.svg',
  },
  {
    type: 'button',
    name: 'Analytics',
    href: '/analytics',
    img: '/img/sideMenu/chart-square.svg',
  },
  {
    type: 'list',
    name: 'E-commerce',
    img: '/img/sideMenu/Iconebox.svg',
    list: [
      {
        name: 'Orders',
        href: '/orders',
      },
      {
        name: 'Customers',
        href: '/customers',
      },
      {
        name: 'Invoices',
        href: '/invoices',
      },
    ],
  },
]
export const apps = [
  {
    type: 'list',
    name: 'Finance',
    img: '/img/sideMenu/empty-wallet.svg',
    list: [],
  },
  {
    type: 'button',
    name: 'Messages',
    href: '/messages',
    img: '/img/sideMenu/messages-2.svg',
  },
  {
    type: 'button',
    name: 'Calendar',
    href: '/calendar',
    img: '/img/sideMenu/calendar.svg',
  },
  {
    type: 'button',
    name: 'Tasks',
    href: '/tasks',
    img: '/img/sideMenu/Notes.svg',
  },
]
