'use client'
import classes from './list.module.scss'
import Link from 'next/link'
import { useEffect, useRef, useState } from 'react'
import Image from 'next/image'
import autoAnimate from '@formkit/auto-animate'
import { usePathname, useRouter } from 'next/navigation'

const List = ({
  list,
  name,
  src,
}: {
  list?: Array<{ name: string; href: string }>
  name: string
  src: string
}) => {
  const [seeList, setSeeList] = useState(false)
  const handleList = () => {
    setSeeList((prev) => !prev)
  }
  const parent = useRef(null)

  useEffect(() => {
    parent.current &&
      autoAnimate(parent.current, {
        duration: 300,
        easing: 'ease-in-out',
        disrespectUserMotionPreference: false,
      })
  }, [parent])
  const path = usePathname()

  return (
    <div
      className={classes.list + (seeList ? ' ' + classes.active : '')}
      ref={parent}
    >
      <button onClick={handleList}>
        <Image src={src} alt={name} width={24} height={24} /> {name}
        <Image src="/img/Chevron_Down.svg" alt="arrow" width={24} height={24} />
      </button>
      {seeList &&
        list?.map((e, i) => {
          return (
            <Link
              key={i}
              href={e.href}
              className={
                classes.listButton +
                ' ' +
                (path === e.href ? classes.linkActive : '')
              }
            >
              <span>{e.name}</span>
            </Link>
          )
        })}
    </div>
  )
}

export default List
