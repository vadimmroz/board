'use client'
import classes from './button.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import { usePathname } from 'next/navigation'

const Button = ({
  name,
  href,
  src,
}: {
  name: string
  href?: string
  src: string
}) => {
  const path = usePathname()

  return (
    <Link
      href={href || ''}
      className={classes.button + ' ' + (path === href ? classes.active : '')}
    >
      <Image src={src} alt={name} width={24} height={24} />
      {name}
    </Link>
  )
}

export default Button
