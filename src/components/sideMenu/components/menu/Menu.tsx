import classes from './menu.module.scss'
import { ReactNode } from 'react'
const Menu = ({ children, name }: { children?: ReactNode; name: string }) => {
  return <div className={classes.menu}>{children}</div>
}

export default Menu
