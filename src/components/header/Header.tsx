'use client'
import classes from './header.module.scss'
import SearchInput from './components/searchInput'
import { ChangeEvent, useState } from 'react'
import Control from '@/components/header/components/control'
import { Coda } from 'next/dist/compiled/@next/font/dist/google'

const Header = () => {
  const [search, setSearch] = useState('')
  const handleSearch = (e: ChangeEvent<HTMLInputElement>) =>
    setSearch(e.target.value)

  return (
    <header
      className={classes.header}
      draggable
      onDragEnd={(e) => console.log(e)}
    >
      <SearchInput
        type="text"
        placeholder="Search..."
        value={search}
        setter={handleSearch}
      />
      <Control />
    </header>
  )
}

export default Header
