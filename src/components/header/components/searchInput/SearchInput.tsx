import { ChangeEvent } from 'react'
import classes from './searchInput.module.scss'
import Image from 'next/image'

type Props = {
  type: string
  placeholder: string
  value: string | number
  setter: (e: ChangeEvent<HTMLInputElement>) => void
}
const SearchInput = ({ type, placeholder, value, setter }: Props) => {
  return (
    <div className={classes.searchInput}>
      <Image src="/img/Search.svg" alt="" width={20} height={20} />
      <input
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={setter}
      />
    </div>
  )
}

export default SearchInput
