'use client'
import classes from './control.module.scss'
import Button from './components/button'
import Person from './components/person'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { IUserDto } from '@/dto/IUserDto'
import { usePathname, useRouter } from 'next/navigation'
import { url } from '@/utils/utils'

const Control = () => {
  const [userData, setUserData] = useState<IUserDto | null>(null)
  const router = useRouter()
  const path = usePathname()
  useEffect(() => {
    const getData = () => {
      const token = localStorage.getItem('token') || ''
      if (token) {
        axios
          .get(`${url}/auth/profile`, {
            headers: { Authorization: `Bearer ${token}` },
          })
          .then((res) => {
            if (res.data) {
              setUserData(res.data[0])
            }
          })
          .catch(() => {
            router.push('/login')
          })
      } else {
        router.push('/login')
      }
    }
    getData()
  }, [path])
  return (
    <div className={classes.control}>
      <div className={classes.bar}>
        <Button src="/img/notification.svg" alt="notification" />
        <Button src="/img/Info.svg" alt="info" />
      </div>
      <Person src="/img/testUser.png" alt="avatar" name="Erik Brown" />
    </div>
  )
}

export default Control
