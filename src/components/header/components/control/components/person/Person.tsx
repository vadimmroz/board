'use client'
import classes from './person.module.scss'
import Image from 'next/image'
import { useEffect, useRef, useState } from 'react'
import { set } from 'immutable'
import autoAnimate from '@formkit/auto-animate'
import Menu from '@/components/sideMenu/components/menu'
import Button from '@/components/sideMenu/components/button'
import LogoutButton from '@/components/UIKit/button'
import { usePathname, useRouter } from 'next/navigation'

const Person = ({
  src,
  alt,
  name,
}: {
  src: string
  alt: string
  name: string
}) => {
  const [menuIsVisible, setMenuIsVisible] = useState(false)

  const handlePerson = () => {
    setMenuIsVisible((prev) => !prev)
  }
  const parent = useRef(null)
  const router = useRouter()

  const handleLogout = () => {
    localStorage.removeItem('token')
    router.push('/')
    setMenuIsVisible(false)
  }
  const path = usePathname()

  useEffect(() => {
    setMenuIsVisible(false)
  }, [path])

  useEffect(() => {
    parent.current &&
      autoAnimate(parent.current, {
        duration: 300,
        easing: 'ease-in-out',
        disrespectUserMotionPreference: false,
      })
  }, [parent])

  return (
    <div className={classes.personContainer} ref={parent}>
      <button className={classes.person} onClick={handlePerson}>
        <Image
          src={src}
          alt={alt}
          width={36}
          height={36}
          className={classes.avatar}
        />
        <h3>{name}</h3>
        <Image src="/img/Chevron_Down.svg" alt="arrow" width={24} height={24} />
      </button>
      {menuIsVisible && (
        <div className={classes.menu}>
          <Menu name={name}>
            <Image
              src="/img/testUser.png"
              width={60}
              height={60}
              alt={'person'}
            />
            <Button name="Profile" href="/profile" src="/img/user.svg" />
            <Button name="Notifications" src="/img/notification.svg" />
            <LogoutButton
              size="medium"
              color="danger"
              disabled={false}
              variant=""
              bordered
              onClick={handleLogout}
            >
              Logout
            </LogoutButton>
          </Menu>
        </div>
      )}
    </div>
  )
}

export default Person
