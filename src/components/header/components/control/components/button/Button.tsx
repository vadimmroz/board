import classes from './button.module.scss'
import Image from 'next/image'

const Button = ({ src, alt }: { src: string; alt: string }) => {
  return (
    <button className={classes.button}>
      <Image src={src} alt={alt} width={24} height={24} />
    </button>
  )
}

export default Button
