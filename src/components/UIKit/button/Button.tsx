import classes from './button.module.scss'
import { MouseEventHandler, ReactNode } from 'react'
import classNames from 'classnames'

type ButtonProps = {
  children: ReactNode
  size: 'small' | 'medium' | 'big' | 'icon'
  color: 'default' | 'danger' | 'success' | 'iconic'
  disabled?: boolean
  variant?: '' | 'outlined'
  bordered?: boolean
  onClick?: MouseEventHandler<HTMLButtonElement> | undefined
}

const Button = ({
  children,
  size,
  color,
  disabled,
  variant,
  bordered,
  onClick,
}: ButtonProps) => {
  return (
    <button
      disabled={disabled}
      className={classNames(
        classes.button,
        classes[size],
        classes[color],
        classes[variant || ''],
        bordered && classes.bordered
      )}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default Button
