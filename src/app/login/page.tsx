import LoginForm from '@/plugins/login/loginForm'

const Login = () => {
  return (
    <div>
      <LoginForm />
    </div>
  )
}

export default Login
