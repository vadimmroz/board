import axios from 'axios'
import { url } from '@/utils/utils'
import ITask from '@/plugins/tasks/list/card/ITask'
import React from 'react'
import Task from '@/plugins/tasks/task'

type Props = {
  params: {
    id: string
  }
}

const TaskPage = async ({ params }: Props) => {
  const { data } = await axios.get<ITask>(`${url}/todo-task/${params.id}`)
  return <Task {...data} />
}

export default TaskPage
