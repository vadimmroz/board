import { ReactNode } from 'react'
import TasksPage from '@/app/tasks/page'
import Portal from '@/components/portal'

export default function RootLayout({
  children,
}: Readonly<{
  children: ReactNode
}>) {
  return (
    <>
      <TasksPage />
      <Portal>{children}</Portal>
    </>
  )
}
