'use server'
import React from 'react'
import ITask from '@/plugins/tasks/list/card/ITask'
import axios from 'axios'
import Tasks from '@/plugins/tasks/tasks'
import { TList } from '@/plugins/tasks/list/TList'
import { url } from '@/utils/utils'
import { revalidatePath } from 'next/cache'

const TasksPage = async () => {
  let lists: TList[] = []
  let cards: ITask[] = []
  try {
    const request = await axios.get(`${url}/todo-list/list`)
    lists = request.data
  } catch (err) {
    console.log(err)
  }
  try {
    const request = await axios.get(`${url}/todo-task`)
    cards = request.data
  } catch (err) {
    console.log(err)
  }
  revalidatePath('tasks')

  return <Tasks lists={lists} cards={cards} />
}

export default TasksPage
