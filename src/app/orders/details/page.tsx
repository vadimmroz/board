'use client'
import { useRouter } from 'next/navigation'
import { useEffect } from 'react'
import Preloader from '@/components/preloader'

export default function Page() {
  const router = useRouter()
  useEffect(() => {
    router.push('/orders')
  }, [router])
  return <Preloader />
}
