import PageHeader from '@/components/pageHeader'
import axios from 'axios'
import { url } from '@/utils/utils'
import OrderDetails from '@/plugins/orders/details'
import { IOrder } from '@/plugins/orders/utils'
import { revalidatePath } from 'next/cache'

type Props = {
  params: {
    id: number
  }
}

const Page = async ({ params }: Props) => {
  const id = params.id
  const { data } = await axios.get<IOrder>(`${url}/orders/${id}`)
  return (
    <>
      <PageHeader>
        <h2>Order Detail</h2>
      </PageHeader>
      <OrderDetails data={data} />
    </>
  )
}

export default Page
