import PageHeader from '@/components/pageHeader'
import React from 'react'
import axios from 'axios'
import { url } from '@/utils/utils'
import { IOrder } from '@/plugins/orders/utils'
import Orders from '@/plugins/orders'
import Button from '@/components/UIKit/button'
import Link from 'next/link'
import { revalidatePath } from 'next/cache'

const OrdersPage = async () => {
  const { data } = await axios.get<IOrder[]>(`${url}/orders`)
  revalidatePath('orders')
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <PageHeader>
        <h2>Orders</h2>
        <Button size="medium" color="default">
          <Link href={'/orders/add'}>+ Add Order</Link>
        </Button>
      </PageHeader>
      <Orders data={data} />
    </div>
  )
}

export default OrdersPage
