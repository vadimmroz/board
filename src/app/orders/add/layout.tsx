import { ReactNode } from 'react'
import Portal from '@/components/portal'
import OrdersPage from '@/app/orders/page'

export default function RootLayout({
  children,
}: Readonly<{
  children: ReactNode
}>) {
  return (
    <>
      <OrdersPage />
      <Portal>{children}</Portal>
    </>
  )
}
