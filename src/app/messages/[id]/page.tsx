import Messages from '../../../plugins/messages'
import Header from '@/plugins/messages/components/header'

type Props = {
  params: {
    id: string
  }
}

const Page = ({ params }: Props) => {
  return (
    <div>
      <Header id={params.id} />
      <Messages id={params.id} />
    </div>
  )
}

export default Page
