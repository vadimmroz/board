import classes from './dashboard.module.scss'
import Button from '@/components/UIKit/button'
import Image from 'next/image'
import DashboardDND from '@/components/dashboardDND'
import PageHeader from '@/components/pageHeader'
import { lazy, Suspense } from 'react'

const TotalEarning = lazy(() => import('@/plugins/dashboard/totalEarning'))
const Orders = lazy(() => import('@/plugins/dashboard/orders'))
const Customers = lazy(() => import('@/plugins/dashboard/customers'))
const Balance = lazy(() => import('@/plugins/dashboard/balance'))
const SalesAnalytics = lazy(() => import('@/plugins/dashboard/salesAnalytics'))
const Map = lazy(() => import('@/plugins/dashboard/map'))
const InformationByStores = lazy(
  () => import('@/plugins/dashboard/InformationByStores')
)

const Dashboard = () => {
  return (
    <>
      <PageHeader>
        <h2>Dashboard</h2>
        <div className={classes.menu}>
          <Button size="small" color="default">
            + Add Product
          </Button>
          <Button size="small" color="default" variant="outlined" bordered>
            <Image
              src="/img/line_chart_up.svg"
              alt="list-filter"
              width={24}
              height={24}
            />
          </Button>
        </div>
      </PageHeader>
      <DashboardDND>
        <Suspense>
          <TotalEarning />
          <Orders />
          <Customers />
          <Balance />
          <SalesAnalytics />
          <Map />
          <InformationByStores />
        </Suspense>
      </DashboardDND>
    </>
  )
}

export default Dashboard
