import classes from './header.module.scss'
import { Flex, Input, Typography } from 'antd'
import Image from 'next/image'

const Header = ({ id }: { id: string }) => {
  return (
    <Flex className={classes.header} gap={67}>
      <div className={classes.search}>
        <Image width={24} height={24} src="/img/Search.svg" alt="search" />
        <Input placeholder="Search by user name" />
      </div>
      <div className={classes.people}>
        <Image src={'/img/testUser.png'} alt="people" width={50} height={50} />
        <div>
          <Typography>{id}</Typography>
          <Typography>Director of the Lviv Forum</Typography>
        </div>
      </div>
    </Flex>
  )
}

export default Header
