'use client'

import classes from './messages.module.scss'
import { useCreateConnection } from '@/plugins/messages/utils'
import { Button, Flex, Input } from 'antd'
import { useState } from 'react'
import { WebsocketService } from '@/utils/WebsocketService'
const Messages = ({ id }: { id: string }) => {
  const [text, setText] = useState('')
  const { messages, author, setAuthor } = useCreateConnection(id)
  const handleSend = () => {
    WebsocketService.socket?.emit('messages', {
      name: id,
      text: text,
      author: author,
    })
  }
  return <div className={classes.messages}>123</div>
}

export default Messages
