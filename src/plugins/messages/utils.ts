import { useEffect, useState } from 'react'
import { WebsocketService } from '@/utils/WebsocketService'

export interface IMessage {
  name: string
  author: string
  date: Date
  status: string
}

export const useCreateConnection = (id: string) => {
  const [messages, setMessages] = useState<string[]>([])
  const [author, setAuthor] = useState('vadim')

  const connectSocket = () => {
    WebsocketService.createConnection()
    WebsocketService.socket?.on(id, (data) => {
      setMessages((prev) => [...prev, `${data.author}: ${data.text}`])
    })
  }

  useEffect(() => {
    connectSocket()
  }, [])

  return { messages, author, setAuthor }
}
