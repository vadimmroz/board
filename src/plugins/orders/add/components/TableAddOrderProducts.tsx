import { Table, TableProps, Typography } from 'antd'
import { IProduct } from '@/plugins/orders/utils'
import classes from '@/plugins/orders/details/components/orderTable/table.module.scss'
import Image from 'next/image'
import React from 'react'
import { dataParse } from '@/plugins/orders/add/utils'

type DataType = {
  name: { name: string; img: string }
  price: number
  total: number
  id?: string
}

const TableAddOrderProducts = ({
  products,
  deleteProduct,
}: {
  products: IProduct[]
  deleteProduct: (e: string) => void
}) => {
  const columns: TableProps<DataType>['columns'] = [
    {
      dataIndex: 'name',
      key: 'name',
      title: 'name',
      render: (text) => (
        <div className={classes.name}>
          <Image src={text.img} alt={text.name} width={28} height={28} />
          {text.name}
        </div>
      ),
    },
    {
      dataIndex: 'price',
      key: 'price',
      title: 'Price',
    },
    {
      dataIndex: 'total',
      key: 'total',
      title: 'Quantity',
    },
    {
      dataIndex: 'total_price',
      key: 'total_price',
      title: 'Total',
      render: (_, arr) => <Typography>{arr.price * arr.total}</Typography>,
    },
    {
      dataIndex: 'delete',
      key: 'delete',
      title: (_) => (
        <Image
          src="/img/delete-svgrepo-com.svg"
          alt="delete"
          width={20}
          height={20}
        />
      ),
      render: (_, prod) => (
        <button
          className={classes.button}
          onClick={() => {
            deleteProduct(prod.id || '')
          }}
        >
          <Image
            src="/img/delete-svgrepo-com.svg"
            alt="delete"
            width={20}
            height={20}
          />
        </button>
      ),
    },
  ]

  return <Table dataSource={dataParse(products)} columns={columns} />
}

export default TableAddOrderProducts
