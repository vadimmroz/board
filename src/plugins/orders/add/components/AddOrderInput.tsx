import React from 'react'
import { Input, Typography } from 'antd'
import classes from '../addOrders.module.scss'

type Props = {
  handleField: (value: string | number | Date, name: string) => void
  name: string
  value: string | number
  placeholder: string
  type: string
}

const AddOrderInput = ({
  handleField,
  name,
  value,
  placeholder,
  type,
}: Props) => {
  return (
    <div className={classes.input}>
      <Typography>{placeholder}:</Typography>
      <Input
        type={type}
        onChange={(e) => {
          handleField(e.target.value, name)
        }}
        value={value}
        placeholder={placeholder}
      />
    </div>
  )
}

export default AddOrderInput
