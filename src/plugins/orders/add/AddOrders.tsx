'use client'
import classes from './addOrders.module.scss'
import { useState } from 'react'
import { IOrder, IProduct } from '@/plugins/orders/utils'
import { Button, Select, Typography } from 'antd'
import AddOrderInput from '@/plugins/orders/add/components/AddOrderInput'
import axios from 'axios'
import { url } from '@/utils/utils'
import { useRouter } from 'next/navigation'
import TableAddOrderProducts from '@/plugins/orders/add/components/TableAddOrderProducts'

const AddOrders = () => {
  const [form, setForm] = useState<IOrder>({
    name: '',
    email: '',
    phone: '',
    address: '',
    businessPhone: '',
    businessName: '',
    paymentCardNumber: '',
    paymentCardType: '',
    pay: '',
    paymentType: '',
    status: '',
    shipping: '',
    notes: '',
    total: 0,
    date: new Date(),
    items: 0,
  })
  const [products, setProducts] = useState<IProduct[]>([])
  const [product, setProduct] = useState<IProduct>({
    count: 0,
    id: Math.random().toString(),
    img: '',
    orderId: '',
    prise: 0,
    name: '',
    total: '',
  })

  const handleFieldForm = (value: string | number | Date, name: string) => {
    setForm({ ...form, [name]: value })
  }
  const handleFieldProducts = (value: string | number | Date, name: string) => {
    setProduct({ ...product, [name]: value })
  }
  const router = useRouter()

  const addOrder = () => {
    let total = 0
    let items = 0
    products.forEach((e) => {
      total += Number(e.count) * Number(e.prise)
      items += Number(e.count)
    })
    axios
      .post(`${url}/orders`, { ...form, items: items, total: total })
      .then((res) => {
        axios
          .post(
            `${url}/products`,
            products.map((e) => {
              return {
                count: Number(e.count),
                img: e.img,
                orderId: res.data.id,
                prise: Number(e.prise),
                name: e.name,
                total: (e.count * e.prise).toString(),
              }
            })
          )
          .then(() => {
            router.push('/orders')
          })
      })
  }
  const deleteProduct = (id: string) => {
    setProducts(products.filter((e) => e.id !== id))
  }
  const handleAddProduct = () => {
    setProducts((prev) => [...prev, product])
    setProduct({
      name: '',
      orderId: '',
      prise: 0,
      img: '',
      total: '',
      count: 0,
      id: Math.random().toString(),
    })
  }
  return (
    <div className={classes.cover}>
      <form
        onSubmit={(e) => {
          e.preventDefault()
          e.stopPropagation()
        }}
      >
        <button className={classes.close}>&times;</button>
        <section className={classes.page}>
          <article className={classes.block}>
            <Typography>Order Info</Typography>
            <AddOrderInput
              type="text"
              placeholder="Name"
              value={form.name}
              name="name"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="email"
              placeholder="Email"
              value={form.email}
              name="email"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="tel"
              placeholder="Phone"
              value={form.phone}
              name="phone"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="text"
              placeholder="Adress"
              value={form.address}
              name="address"
              handleField={handleFieldForm}
            />
          </article>
          <article className={classes.block}>
            <Typography>Card Info</Typography>
            <AddOrderInput
              type="text"
              placeholder="Busines name"
              value={form.businessName}
              name="businessName"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="tel"
              placeholder="Busines phone"
              value={form.businessPhone}
              name="businessPhone"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="number"
              placeholder="Payment card number"
              value={form.paymentCardNumber}
              name="paymentCardNumber"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="text"
              placeholder="Payment card type"
              value={form.paymentCardType}
              name="paymentCardType"
              handleField={handleFieldForm}
            />
            <AddOrderInput
              type="text"
              placeholder="Payment type"
              value={form.paymentType}
              name="paymentType"
              handleField={handleFieldForm}
            />
          </article>
          <article className={classes.block}>
            <Typography>Status: </Typography>
            <Select
              placeholder="status"
              onSelect={(e) => handleFieldForm(e, 'status')}
              value={form.status}
            >
              <Select.Option key="Refunded">Refunded</Select.Option>
              <Select.Option key="Approved">Approved</Select.Option>
              <Select.Option key="Unpaid">Unpaid</Select.Option>
            </Select>
          </article>
        </section>
        <section className={classes.page}>
          <article className={classes.block}>
            <Typography>Product Info</Typography>
            <AddOrderInput
              handleField={handleFieldProducts}
              name="name"
              value={product.name}
              placeholder="Product name"
              type="text"
            />
            <AddOrderInput
              handleField={handleFieldProducts}
              name="img"
              value={product.img}
              placeholder="Product img"
              type="text"
            />
            <AddOrderInput
              handleField={handleFieldProducts}
              name="prise"
              value={product.prise}
              placeholder="Product prise"
              type="number"
            />
            <AddOrderInput
              handleField={handleFieldProducts}
              name="count"
              value={product.count}
              placeholder="Product count"
              type="number"
            />
          </article>
          <Button onClick={handleAddProduct}>Add Product</Button>
          <article className={classes.block}>
            <Typography>Products:</Typography>
            <TableAddOrderProducts
              products={products}
              deleteProduct={deleteProduct}
            />
          </article>
        </section>
        <Button className={classes.addButton} onClick={addOrder}>
          Add Order
        </Button>
      </form>
    </div>
  )
}

export default AddOrders
