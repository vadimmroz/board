import { IProduct } from '@/plugins/orders/utils'

export const dataParse = (data: IProduct[]) => {
  return data.map((e) => {
    return {
      name: {
        name: e.name,
        img: e.img,
      },
      price: e.prise,
      total: e.count,
      id: e.id,
    }
  })
}
