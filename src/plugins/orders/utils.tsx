'use client'
import React from 'react'
import { TableProps, Typography } from 'antd'
import classes from '@/plugins/orders/details/components/orderTable/table.module.scss'
import Image from 'next/image'
import Link from 'next/link'
import moment from 'moment'

export type DataType = {
  name: { tag: string; img: string }
  date: Date
  customer: string
  total: number
  key: string
  status: string
  items: number
  location: string
  paymentType: string
}

export interface IOrder {
  date: Date
  address: string
  businessPhone: string
  businessName: string
  email: string
  name: string
  notes: string
  pay: string
  paymentCardNumber: string
  paymentCardType: string
  phone: string
  shipping: string
  status: string
  id?: string
  total: number
  items: number
  paymentType: string
}
export interface IProduct {
  id?: string
  name: string
  img: string
  prise: number
  count: number
  total: string
  orderId: string | null
}

export const defaultData = [
  {
    name: { tag: '134678', img: '/img/telephone.png' },
    date: new Date(),
    customer: 'Maria Sretnenko',
    total: 130,
    key: '1',
    status: 'Refunded',
    items: 1,
    location: 'Ivano-Frankivsk',
    paymentType: 'Subscription',
  },
  {
    name: { tag: '144674', img: '/img/telephone.png' },
    date: new Date(),
    customer: 'katerinpopp',
    total: 15,
    key: '2',
    status: 'Approved',
    items: 2,
    location: 'Kyiv',
    paymentType: 'Subscription',
  },
  {
    name: { tag: '144876', img: '/img/telephone.png' },
    date: new Date(),
    customer: 'ANNAfONENKO',
    total: 1556,
    key: '3',
    status: 'Approved',
    items: 2,
    location: 'Lviv',
    paymentType: 'One Time',
  },
  {
    name: { tag: '164675', img: '/img/telephone.png' },
    date: new Date(),
    customer: 'Roman Ketrin',
    total: 23,
    key: '4',
    status: 'Refunded',
    items: 2,
    location: 'Bilytske, Donetsk ',
    paymentType: 'Subscription',
  },
]

export const columns: TableProps<DataType>['columns'] = [
  {
    title: 'Order',
    dataIndex: 'name',
    key: 'name',
    render: (text) => (
      <Link href={'/orders/details/' + text.tag} className={classes.name}>
        <Image src={text.img} alt={text.tag} width={28} height={28} />#
        {text.tag}
      </Link>
    ),
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
    render: (text) => (
      <Typography>{moment(text).format('DD/MM/yyyy')}</Typography>
    ),
    sorter: (a, b) => Number(a.date) - Number(b.date),
  },
  {
    title: 'Customer',
    dataIndex: 'customer',
    key: 'customer',
  },
  {
    title: 'TOTAL',
    key: 'total',
    dataIndex: 'total',
    render: (text) => <Typography>{text}₴</Typography>,
    sorter: (a, b) => a.total - b.total,
  },
  {
    title: 'Status',
    key: 'status',
    dataIndex: 'status',
    filters: [
      {
        text: 'Refunded',
        value: 'Refunded',
      },
      {
        text: 'Approved',
        value: 'Approved',
      },
      {
        text: 'Unpaid',
        value: 'Unpaid',
      },
    ],
    filterMode: 'tree',
    filterSearch: true,
    onFilter: (value, record) => record.status.startsWith(value as string),
  },
  {
    title: 'Items',
    key: 'items',
    dataIndex: 'items',
    sorter: (a, b) => a.items - b.items,
  },
  {
    title: 'Location',
    key: 'location',
    dataIndex: 'location',
  },
  {
    title: 'Payment Type',
    key: 'paymentType',
    dataIndex: 'paymentType',
    filters: [
      {
        text: 'One Time',
        value: 'OneTime',
      },
      {
        text: 'Subscription',
        value: 'Subscription',
      },
    ],
    filterMode: 'tree',
    filterSearch: true,
    onFilter: (value, record) => record.paymentType.startsWith(value as string),
  },
]

export const parseData = (data: IOrder[]): DataType[] => {
  return data.map((e, i) => {
    return {
      name: { tag: e.id || '', img: '/img/telephone.png' },
      date: e.date,
      customer: e.businessName,
      total: e.total,
      key: e.id || i.toString(),
      status: e.status,
      items: e.items,
      location: e.address,
      paymentType: e.paymentType,
    }
  })
}
