'use client'
import React from 'react'
import { TableProps, Typography } from 'antd'
import classes from '@/plugins/orders/details/components/orderTable/table.module.scss'
import Image from 'next/image'
import PageHeader from '@/components/pageHeader'
import { IProduct } from '@/plugins/orders/utils'

export interface DataType {
  key: string
  name: { name: string; img: string }
  price: number
  total: number
  count: number
}

export const columns: TableProps<DataType>['columns'] = [
  {
    title: 'NAME',
    dataIndex: 'name',
    key: 'name',
    render: (text) => (
      <a className={classes.name}>
        <Image src={text.img} alt={text.name} width={28} height={28} />
        {text.name}
      </a>
    ),
  },
  {
    title: 'PRICE',
    dataIndex: 'price',
    key: 'price',
    render: (text) => <Typography>{text}₴</Typography>,
  },
  {
    title: 'COUNT',
    dataIndex: 'count',
    key: 'count',
  },
  {
    title: 'TOTAL',
    key: 'total',
    dataIndex: 'total',
    render: (_, field) => <p>{field.price * field.count}₴</p>,
  },
]

export const defaultData = [
  {
    name: { name: 'Xiaomi Redmi Note 11', img: '/img/telephone.png' },
    price: 8599,
    count: 1,
    total: 8599,
    key: '1',
  },
  {
    name: {
      name: 'Xiaomi Mi 23.8" Desktop Monitor 1C (BHR4510GL-1)',
      img: '/img/telephone.png',
    },
    price: 5499,
    count: 12,
    total: 5499 * 12,
    key: '2',
  },
  {
    name: { name: 'Xiaomi Watch S1 Black', img: '/img/telephone.png' },
    price: 5499,
    count: 12,
    total: 5499 * 12,
    key: '3',
  },
  {
    name: {
      name: 'Xiaomi Mi 23.8" DesktopBHR4510GL-1)',
      img: '/img/telephone.png',
    },
    price: 5499,
    count: 12,
    total: 5499 * 12,
    key: '4',
  },
]

export const Footer = ({ children }: { children: string }) => {
  return (
    <PageHeader>
      <p>Summary</p>
      <p>{children}</p>
    </PageHeader>
  )
}

export const dataParse = (data: IProduct[]): DataType[] => {
  return data.map((e) => {
    return {
      key: e.id || '',
      name: { name: e.name, img: e.img },
      price: e.prise,
      total: e.count * e.prise,
      count: e.count,
    }
  })
}
