'use client'
import React, { useEffect, useState } from 'react'
import { Table } from 'antd'
import { columns, DataType, dataParse } from './utils'
import classes from './table.module.scss'
import axios from 'axios'
import { url } from '@/utils/utils'
import { IProduct } from '@/plugins/orders/utils'
import PageHeader from '@/components/pageHeader'
import Button from '@/components/UIKit/button'

const OrderTable = ({ id }: { id: string }) => {
  const [data, setData] = useState<DataType[]>([])
  const [selectedProducts, setSelectedProducts] = useState<string[]>([])
  useEffect(() => {
    const getData = () => {
      axios
        .get<IProduct[]>(`${url}/products/getAllProductsByOrderId/${id}`)
        .then((res) => {
          setData(dataParse(res.data))
        })
    }
    getData()
  }, [id])
  const rowSelection = {
    onChange: (_: React.Key[], selectedRows: DataType[]) => {
      setSelectedProducts([...selectedRows.map((e) => e.key)])
    },
  }
  const handleDeleteProducts = () => {
    axios
      .delete(`${url}/products/deleteManyProducts`, { data: selectedProducts })
      .then(() => {
        setData(data.filter((e) => !selectedProducts.includes(e.key)))
        setSelectedProducts([])
      })
  }
  return (
    <div className={classes.products}>
      <PageHeader>
        <h6>Products</h6>
        <div className={classes.orderControl}>
          <p>{selectedProducts.length} selected products</p>
          <Button
            onClick={handleDeleteProducts}
            size="medium"
            color="danger"
            variant="outlined"
            bordered
            disabled={selectedProducts.length < 1}
          >
            Delete
          </Button>
        </div>
      </PageHeader>
      <Table
        rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}
        pagination={false}
        columns={columns}
        dataSource={data}
        summary={(data) => {
          let totalCount = 0
          let totalPrice = 0
          data.forEach((e) => {
            totalCount += e.count
            totalPrice += e.total
          })
          return (
            <Table.Summary.Row>
              <Table.Summary.Cell index={0}>
                <p className={classes.summary}>Summary</p>
              </Table.Summary.Cell>
              <Table.Summary.Cell index={1}></Table.Summary.Cell>
              <Table.Summary.Cell index={2}></Table.Summary.Cell>
              <Table.Summary.Cell index={3}>
                <p className={classes.summary}>{totalCount}</p>
              </Table.Summary.Cell>
              <Table.Summary.Cell index={4}>
                <p className={classes.summary}>{totalPrice}₴</p>
              </Table.Summary.Cell>
            </Table.Summary.Row>
          )
        }}
      />
    </div>
  )
}

export default OrderTable
