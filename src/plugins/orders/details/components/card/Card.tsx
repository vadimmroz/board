import classes from './card.module.scss'
import Image from 'next/image'
import TextField from '@/plugins/orders/details/components/card/components/TextField'

type Props = {
  img: string
  title: string
  fields: { name: string; value: string | number }[]
}

const Card = ({ img, title, fields }: Props) => {
  return (
    <article className={classes.card}>
      <div className={classes.circle}>
        <Image src={img} alt="person" width={24} height={24} />
      </div>
      <div className={classes.text}>
        <h6>{title}</h6>
        {fields.map((e) => {
          return e.name === 'notes' ? (
            <TextField value={e.value} />
          ) : (
            <p key={Math.random()}>
              <span>{e.name}: </span>
              {e.value}
            </p>
          )
        })}
      </div>
    </article>
  )
}

export default Card
