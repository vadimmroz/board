'use client'

import { ChangeEvent, useState } from 'react'
import TextArea from 'antd/es/input/TextArea'
import Button from '@/components/UIKit/button'

const TextField = ({ value }: { value: string | number }) => {
  const [text, setText] = useState(value)
  const handleText = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value)
  }
  return (
    <>
      <TextArea
        value={text}
        onChange={handleText}
        placeholder="Type some note..."
      />
      <Button size="medium" color="default" variant="outlined" bordered>
        Save
      </Button>
    </>
  )
}

export default TextField
