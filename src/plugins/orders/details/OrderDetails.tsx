import classes from './orderDetails.module.scss'
import { IOrder } from '@/plugins/orders/utils'
import moment from 'moment'
import PageHeader from '@/components/pageHeader'
import Button from '@/components/UIKit/button'
import Image from 'next/image'
import Card from '@/plugins/orders/details/components/card'
import OrderTable from './components/orderTable'

const OrderDetails = ({ data }: { data: IOrder }) => {
  return (
    <section className={classes.orderDetails}>
      <PageHeader>
        <div className={classes.info}>
          <h6>Order ID #{data.id}</h6>
          <p>{moment(data.date).format('DDD, MMMM DD,YYYY')}</p>
          <button className={classes[data.status]}>{data.status}</button>
        </div>
        <div className={classes.menu}>
          <Button size="medium" color="default" disabled>
            Change status
          </Button>
          <Button size="medium" color="default" disabled>
            Save
          </Button>
          <Button size="medium" color="default" variant="outlined" bordered>
            Download Info
          </Button>
          <Button size="icon" color="iconic">
            <Image src="/img/Print.svg" alt="kjssv" width={24} height={24} />
          </Button>
        </div>
      </PageHeader>
      <div className={classes.content}>
        <Card
          img="/img/user_black.svg"
          title="Customer"
          fields={[
            { name: 'Name', value: data.name },
            { name: 'Email', value: data.email },
            { name: 'Phone', value: data.phone },
          ]}
        />
        <Card
          img="/img/user_black.svg"
          title="Order Info"
          fields={[
            { name: 'Shipping', value: data.shipping },
            { name: 'Pay', value: data.pay },
            { name: 'Status', value: data.status },
          ]}
        />
        <Card
          img="/img/user_black.svg"
          title="Delivery"
          fields={[{ name: 'Address', value: data.address }]}
        />
        <Card
          img="/img/user_black.svg"
          title="Payment Info"
          fields={[
            { name: data.paymentCardType, value: data.paymentCardNumber },
            { name: 'Business Name', value: data.businessName },
            { name: 'Phone', value: data.businessPhone },
          ]}
        />
        <Card
          img="/img/user_black.svg"
          title="Notes"
          fields={[{ name: 'notes', value: data.notes }]}
        />
      </div>
      <OrderTable id={data.id || ''} />
    </section>
  )
}

export default OrderDetails
