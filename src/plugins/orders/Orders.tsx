'use client'
import classes from './orders.module.scss'
import { Table } from 'antd'
import { IOrder, columns, parseData, DataType } from './utils'
import React, { useState } from 'react'

import Button from '@/components/UIKit/button'
import axios from 'axios'
import { url } from '@/utils/utils'

const Orders = ({ data }: { data: IOrder[] }) => {
  const [tableData, setTableData] = useState(parseData(data))
  const [selectedOrders, setSelectedOrders] = useState<string[]>([])
  const rowSelection = {
    onChange: (_: React.Key[], selectedRows: DataType[]) => {
      setSelectedOrders([...selectedRows.map((e) => e.key)])
    },
  }

  const deleteOrders = () => {
    axios
      .delete(`${url}/orders/deleteManyOrders`, { data: selectedOrders })
      .then(() => {
        setTableData(tableData.filter((e) => !selectedOrders.includes(e.key)))
        setSelectedOrders([])
      })
  }
  return (
    <div className={classes.orders}>
      <div className={classes.header}>
        <h5>
          All Orders <span>569</span>
        </h5>
        <div className={classes.control}>
          <p>{selectedOrders.length} item selected</p>
          <Button
            size="medium"
            color="danger"
            variant="outlined"
            onClick={deleteOrders}
            bordered
            disabled={selectedOrders.length < 1}
          >
            Delete
          </Button>
        </div>
      </div>
      <Table
        className={classes.table}
        columns={columns}
        dataSource={tableData}
        rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}
      />
    </div>
  )
}

export default Orders
