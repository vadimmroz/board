import classes from './deleteCard.module.scss'
import { useState } from 'react'
import { Button, Input, Typography } from 'antd'
const DeleteCard = ({
  handleDelete,
  name,
  handleShowDeleteCardModal,
}: {
  handleDelete: () => void
  name: string
  handleShowDeleteCardModal: () => void
}) => {
  const [input, setInput] = useState('')
  return (
    <div className={classes.deleteCard} onClick={handleShowDeleteCardModal}>
      <form
        onClick={(e) => {
          e.preventDefault()
          e.stopPropagation()
        }}
      >
        <button className={classes.close} onClick={handleShowDeleteCardModal}>
          &times;
        </button>
        <div>
          <Typography>
            Please type name of card, that confirm deleting &apos;{name}&apos;:
          </Typography>
          <Input onChange={(e) => setInput(e.target.value)} />
        </div>
        <Button danger disabled={input !== name} onClick={handleDelete}>
          Delete
        </Button>
      </form>
    </div>
  )
}

export default DeleteCard
