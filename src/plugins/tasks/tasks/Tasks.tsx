'use client'
import classes from './tasks.module.scss'
import Button from '@/components/UIKit/button'
import List from '@/plugins/tasks/list'
import React, { useState } from 'react'
import ITask from '@/plugins/tasks/list/card/ITask'
import { TList } from '@/plugins/tasks/list/TList'
import PageHeader from '@/components/pageHeader'
import Portal from '@/components/portal'
import AddList from '@/plugins/tasks/addList'
import classNames from 'classnames'
import { Reorder } from 'framer-motion'

const Tasks = ({ lists, cards }: { lists: TList[]; cards: ITask[] }) => {
  const [cardsData, setCardsData] = useState(cards)
  const [listsData, setListsData] = useState<TList[]>(lists)
  const [addListModal, setAddListModal] = useState(false)

  const handleShowAddTaskList = () => {
    setAddListModal(!addListModal)
  }

  return (
    <section className={classNames(classes.tasks, 'tasks')}>
      <PageHeader>
        <h5>Erik`s tasks</h5>
        <Button
          size={'medium'}
          color={'default'}
          variant={'outlined'}
          onClick={handleShowAddTaskList}
        >
          + Add List
        </Button>
      </PageHeader>

      <div className={classes.taskList}>
        {listsData.map((element, index) => (
          <List
            name={element.name}
            key={index}
            cards={cardsData.filter((card) => card.status === element.name)}
            setListsData={setListsData}
            listsData={listsData}
            list={element}
          />
        ))}
      </div>
      {addListModal && (
        <Portal>
          <AddList
            handleShowAddTaskList={handleShowAddTaskList}
            listsData={listsData}
            setListsData={setListsData}
          />
        </Portal>
      )}
    </section>
  )
}

export default Tasks
