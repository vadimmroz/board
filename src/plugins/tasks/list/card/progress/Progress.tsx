import React from 'react'
import moment from 'moment'
import classes from './progress.module.scss'
import classNames from 'classnames'
import ProgressBar from '@/components/progressBar/ProgressBar'

const Progress = ({ progress, date }: { progress: number; date: Date }) => {
  return (
    <div id="progress" className={classNames(classes.progress)}>
      <div>
        <p className={classes.text}>{progress}% of 100%</p>
        <p className={classes.text}>{moment(date).format('D MMM,YYYY')}</p>
      </div>
      <ProgressBar progress={progress} />
    </div>
  )
}

export default Progress
