export default interface ITask {
  id?: string
  name: string
  author: string
  description: string | ''
  img: string
  progress: number
  owners: string[]
  moders: string[]
  guests: string[]
  priority: string
  status: string
  tags: string[]
  date: Date
  deadline: Date
  index: number
  list?: string[]
}
