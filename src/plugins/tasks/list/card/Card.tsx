'use client'
import ITask from '@/plugins/tasks/list/card/ITask'
import Progress from '@/plugins/tasks/list/card/progress'
import classes from './card.module.scss'
import { Button } from 'antd'
import Image from 'next/image'
import moment from 'moment/moment'
import Link from 'next/link'
import { Dispatch, useState, MouseEvent } from 'react'
import Portal from '@/components/portal'
import DeleteCard from '@/plugins/tasks/deleteCard'
import axios from 'axios'
import { url } from '@/utils/utils'

type Props = {
  card: ITask
  setCardList: Dispatch<ITask[]>
  cardList: ITask[]
}

const Card = ({ card, setCardList, cardList }: Props) => {
  const [deleteTaskShowModal, setDeleteTaskShowModal] = useState(false)
  const parseDesc = (desc: string) => {
    const a = desc.split(' ').map((e, i) => {
      if (e[0] === '#') {
        return (
          <Link href={`/order/${e.slice(1)}`} key={i}>
            {e}{' '}
          </Link>
        )
      }
      return e + ' '
    })
    return <p className={classes.descr}>{a}</p>
  }
  const handleShowDeleteCardModal = (event?: MouseEvent) => {
    setDeleteTaskShowModal(!deleteTaskShowModal)
    if (event) {
      event.preventDefault()
      event.stopPropagation()
    }
  }

  const handleDelete = () => {
    axios.delete(`${url}/todo-task/${card.id}`).then(() => {
      setCardList(cardList.filter((e) => e.id !== card.id))
      handleShowDeleteCardModal()
    })
  }
  return (
    <Link className={classes.card} href={'tasks/' + card.id}>
      <div className={classes.header}>
        <h4>{card.name}</h4>
        <Button danger type="text" onClick={handleShowDeleteCardModal}>
          <Image
            src="/img/delete-svgrepo-com.svg"
            alt="dots"
            width={20}
            height={20}
          />
        </Button>
      </div>
      {parseDesc(card.description)}

      {/*{card.img !== "string" ? <Image width={350} height={170} src={card.img} alt=""/> : ''}*/}

      {card.progress !== -1 ? (
        <Progress progress={card.progress} date={card.date} />
      ) : (
        ''
      )}

      {card.tags.length > 0 && (
        <div className={classes.tags}>
          {card.tags.map((tag, index) => (
            <div key={index} className={classes.tag}>
              {tag}
            </div>
          ))}
        </div>
      )}

      <div className={classes.deadline}>
        <Image
          alt="clock"
          src={'/img/tasksImages/clock.svg'}
          width={18}
          height={18}
        />
        <p className={classes.date}>{moment(card.date).format('D MMM,YYYY')}</p>
      </div>
      {deleteTaskShowModal && (
        <Portal>
          <DeleteCard
            handleDelete={handleDelete}
            name={card.name}
            handleShowDeleteCardModal={handleShowDeleteCardModal}
          />
        </Portal>
      )}
    </Link>
  )
}

export default Card
