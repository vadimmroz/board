'use client'
import React, { Dispatch, useState } from 'react'
import Card from '@/plugins/tasks/list/card'
import ITask from '@/plugins/tasks/list/card/ITask'
import classes from './list.module.scss'
import Button from '@/components/UIKit/button'
import axios from 'axios'
import { url } from '@/utils/utils'
import Portal from '@/components/portal'
import AddCard from '@/plugins/tasks/addCard'
import DeleteList from '@/plugins/tasks/deleteList'
import { TList } from '@/plugins/tasks/list/TList'

type Props = {
  list: TList
  name: string
  cards: ITask[]
  setListsData: Dispatch<TList[]>
  listsData: TList[]
}

const List = ({ name, cards, setListsData, listsData, list }: Props) => {
  const [addCardShowModal, setAddCardShowModal] = useState(false)
  const [cardList, setCardList] = useState<ITask[]>(cards)
  const [deleteListShowModal, setDeleteListShowModal] = useState(false)

  const handleDelete = async () => {
    await axios
      .delete(`${url}/todo-list/list`, { data: { id: list.id } })
      .then(() => {
        setListsData(listsData.filter((e) => e.name !== name))
        handleShowDeleteListModal()
      })
  }

  const handleShowAddCardModal = () => {
    setAddCardShowModal(!addCardShowModal)
  }
  const handleShowDeleteListModal = () => {
    setDeleteListShowModal(!deleteListShowModal)
  }

  return (
    <div className={classes.list}>
      <div className={classes.header}>
        <div>
          <h3>{name}</h3>
          <button onClick={handleShowDeleteListModal}>&times;</button>
        </div>
        <Button
          size={'small'}
          onClick={handleShowAddCardModal}
          color={'default'}
          variant={'outlined'}
        >
          <p className={classes.plus}>+</p>
        </Button>
      </div>
      {cardList.map((card, index) => {
        return (
          <Card
            card={card}
            key={index}
            cardList={cardList}
            setCardList={setCardList}
          />
        )
      })}
      {addCardShowModal && (
        <Portal>
          <AddCard
            handleShowAddCardModal={handleShowAddCardModal}
            status={name}
            cardList={cardList}
            setCardList={setCardList}
          />
        </Portal>
      )}
      {deleteListShowModal && (
        <Portal>
          <DeleteList
            handleDelete={handleDelete}
            name={name}
            handleShowDeleteListModal={handleShowDeleteListModal}
          />
        </Portal>
      )}
    </div>
  )
}

export default List
