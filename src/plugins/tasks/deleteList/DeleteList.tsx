import classes from './deleteList.module.scss'
import { Button, Input, Typography } from 'antd'
import { useState } from 'react'

const DeleteList = ({
  handleDelete,
  name,
  handleShowDeleteListModal,
}: {
  handleDelete: () => void
  name: string
  handleShowDeleteListModal: () => void
}) => {
  const [input, setInput] = useState('')
  return (
    <div className={classes.deleteList} onClick={handleShowDeleteListModal}>
      <form
        onClick={(e) => {
          e.preventDefault()
          e.stopPropagation()
        }}
      >
        <button className={classes.close} onClick={handleShowDeleteListModal}>
          &times;
        </button>
        <div>
          <Typography>
            Please type name of list, that confirm deleting &apos;{name}&apos;:
          </Typography>
          <Input onChange={(e) => setInput(e.target.value)} />
        </div>
        <Button danger disabled={input !== name} onClick={handleDelete}>
          Delete
        </Button>
      </form>
    </div>
  )
}

export default DeleteList
