import classes from './addList.module.scss'
import { Dispatch, useState } from 'react'
import { TList } from '@/plugins/tasks/list/TList'
import axios from 'axios'
import { url } from '@/utils/utils'
import { Button, ColorPicker, Input, Typography } from 'antd'

const AddList = ({
  handleShowAddTaskList,
  listsData,
  setListsData,
}: {
  handleShowAddTaskList: () => void
  listsData: TList[]
  setListsData: Dispatch<TList[]>
}) => {
  const [list, setList] = useState<TList>({
    name: 'string',
    color: '#000',
    tasks: [],
  })
  const handleAddList = async () => {
    await axios.post(`${url}/todo-list/list`, list).then((res) => {
      setListsData([...listsData, res.data])
      handleShowAddTaskList()
    })
  }
  return (
    <article className={classes.addList} onClick={handleShowAddTaskList}>
      <form
        onClick={(e) => {
          e.preventDefault()
          e.stopPropagation()
        }}
      >
        <button className={classes.close} onClick={handleShowAddTaskList}>
          &times;
        </button>
        <Input
          onChange={(e) => setList({ ...list, name: e.target.value })}
          value={list.name}
          placeholder="name"
        />
        <div>
          <Typography>Choose your List Color:</Typography>
          <ColorPicker
            onChange={(e) => setList({ ...list, color: e.toHexString() })}
            value={list.color}
          />
        </div>
        <Button onClick={handleAddList}>Add List</Button>
      </form>
    </article>
  )
}

export default AddList
