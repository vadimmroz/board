'use client'
import classes from './task.module.scss'
import ITask from '@/plugins/tasks/list/card/ITask'
import { MouseEvent, useEffect, useRef, useState, KeyboardEvent } from 'react'
import { useRouter } from 'next/navigation'
import autoAnimate from '@formkit/auto-animate'
import TaskInput from '@/plugins/tasks/task/components/TaskInput'
import TaskTextArea from '@/plugins/tasks/task/components/TaskTextArea'
import TaskSelect from '@/plugins/tasks/task/components/TaskSelect'
import TaskProgress from '@/plugins/tasks/task/components/TaskProgress'
import TaskDate from '@/plugins/tasks/task/components/TaskDate'
import TaskTags from '@/plugins/tasks/task/components/TaskTags'
import axios from 'axios'
import { url } from '@/utils/utils'
import { Keys, dataParser, dataDeParser } from '@/plugins/tasks/task/utils'
import Preloader from '@/components/preloader'

const Task = (data: ITask) => {
  const [editTask, setEditTask] = useState(dataParser(data))
  const [showPreloader, setShowPreloader] = useState(false)
  const prevent = (event: MouseEvent) => {
    event.preventDefault()
    event.stopPropagation()
  }
  const router = useRouter()
  const goBack = (e: MouseEvent) => {
    e.preventDefault()
    router.push('/tasks')
  }
  const handleInput = (
    value: string | number | Date | string[],
    name: Keys
  ) => {
    setEditTask({ ...editTask, [name]: { ...editTask[name], value: value } })
  }

  const parent = useRef(null)
  useEffect(() => {
    parent.current &&
      autoAnimate(parent.current, {
        duration: 300,
        easing: 'ease-in-out',
        disrespectUserMotionPreference: false,
      })
  }, [parent])
  const handleCloseEditMode = (name: Keys) => {
    setEditTask({
      ...editTask,
      [name]: { ...editTask[name], editMode: false },
    })
  }
  const handleKeyPress = (e: KeyboardEvent<HTMLElement>, name: Keys) => {
    if (e.key === 'Enter') {
      setShowPreloader(true)
      axios
        .patch(`${url}/todo-task/${data.id}`, dataDeParser(editTask))
        .then(() => {
          setEditTask({
            ...editTask,
            [name]: { ...editTask[name], editMode: false },
          })
        })
        .finally(() => {
          setShowPreloader(false)
        })
        .catch(() => {
          setEditTask(dataParser(data))
        })
    } else if (e.key === 'Escape') {
      setEditTask(dataParser(data))
    }
  }
  const handleDoubleClick = (name: Keys) => {
    setEditTask({ ...editTask, [name]: { ...editTask[name], editMode: true } })
  }
  return (
    <div className={classes.taskCover}>
      <article className={classes.task} onClick={prevent} ref={parent}>
        <button className={classes.close} onClick={goBack}>
          &times;
        </button>
        <TaskInput
          editMode={editTask.name.editMode}
          value={editTask.name.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="name"
          type="h3"
        />
        <TaskTextArea
          editMode={editTask.description.editMode}
          value={editTask.description.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="description"
          type="p"
        />
        <TaskInput
          editMode={editTask.img.editMode}
          value={editTask.img.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="img"
          type="p"
        />
        <TaskSelect
          editMode={editTask.priority.editMode}
          value={editTask.priority.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="priority"
          type="p"
          selectList={['low', 'medium', 'high']}
        />
        <TaskSelect
          editMode={editTask.status.editMode}
          value={editTask.status.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="status"
          type="p"
          selectList={editTask.list}
        />
        <TaskProgress
          editMode={editTask.progress.editMode}
          value={editTask.progress.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="progress"
          type="p"
        />
        <div className={classes.dateBlock}>
          <TaskDate
            editMode={editTask.date.editMode}
            value={editTask.date.value}
            handleInput={handleInput}
            handleKeyPress={handleKeyPress}
            handleDoubleClick={handleDoubleClick}
            name="date"
            closeEditMode={handleCloseEditMode}
            type="p"
          />
          <TaskDate
            editMode={editTask.deadline.editMode}
            value={editTask.deadline.value}
            handleInput={handleInput}
            handleKeyPress={handleKeyPress}
            handleDoubleClick={handleDoubleClick}
            name="deadline"
            closeEditMode={handleCloseEditMode}
            type="p"
          />
        </div>
        <TaskTags
          editMode={editTask.tags.editMode}
          value={editTask.tags.value}
          handleInput={handleInput}
          handleKeyPress={handleKeyPress}
          handleDoubleClick={handleDoubleClick}
          name="tags"
          closeEditMode={handleCloseEditMode}
          type="div"
        />
      </article>
      {showPreloader && <Preloader className={classes.preloader} />}
    </div>
  )
}

export default Task
