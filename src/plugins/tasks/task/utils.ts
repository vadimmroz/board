import ITask from '@/plugins/tasks/list/card/ITask'

export const dataParser = (data: ITask) => {
  return {
    name: { value: data.name, editMode: false },
    author: { value: data.author, editMode: false },
    date: { value: data.date, editMode: false },
    deadline: { value: data.deadline, editMode: false },
    guests: { value: data.guests, editMode: false },
    id: { value: data.id, editMode: false },
    img: { value: data.img, editMode: false },
    index: { value: data.index, editMode: false },
    moders: { value: data.moders, editMode: false },
    owners: { value: data.owners, editMode: false },
    priority: { value: data.priority, editMode: false },
    progress: { value: data.progress, editMode: false },
    status: { value: data.status, editMode: false },
    tags: { value: data.tags, editMode: false },
    description: { value: data.description, editMode: false },
    list: data.list,
  }
}

export const dataDeParser = (editTask: EditData) => {
  return {
    name: editTask.name.value,
    author: editTask.author.value,
    description: editTask.description.value,
    img: editTask.img.value,
    progress: editTask.progress.value,
    owners: editTask.owners.value,
    moders: editTask.owners.value,
    guests: editTask.guests.value,
    priority: editTask.priority.value,
    status: editTask.status.value,
    tags: editTask.tags.value,
    date: editTask.date.value,
    deadline: editTask.deadline.value,
    index: editTask.index.value,
  }
}

export type Keys =
  | 'name'
  | 'author'
  | 'date'
  | 'deadline'
  | 'guests'
  | 'id'
  | 'img'
  | 'index'
  | 'moders'
  | 'owners'
  | 'priority'
  | 'progress'
  | 'status'
  | 'tags'
  | 'description'

export type EditData = {
  name: { value: string; editMode: boolean }
  author: { value: string; editMode: boolean }
  date: { value: Date; editMode: boolean }
  deadline: { value: Date; editMode: boolean }
  guests: { value: string[]; editMode: boolean }
  id: { value: string | undefined; editMode: boolean }
  img: { value: string; editMode: boolean }
  index: { value: number; editMode: boolean }
  moders: { value: string[]; editMode: boolean }
  owners: { value: string[]; editMode: boolean }
  priority: { value: string; editMode: boolean }
  progress: { value: number; editMode: boolean }
  status: { value: string; editMode: boolean }
  tags: { value: string[]; editMode: boolean }
  description: { value: string; editMode: boolean }
  list?: string[]
}
