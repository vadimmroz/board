import { createElement, KeyboardEvent } from 'react'
import { Select } from 'antd'
import { Keys } from '@/plugins/tasks/task/utils'
import classes from '../task.module.scss'

type Props = {
  editMode: boolean
  value: string
  handleInput: (e: string | number | Date, name: Keys) => void
  handleKeyPress: (e: KeyboardEvent<HTMLElement>, name: Keys) => void
  handleDoubleClick: (name: Keys) => void
  name: Keys
  type: string
  selectList?: string[]
}
const TaskSelect = ({
  editMode,
  value,
  handleInput,
  handleKeyPress,
  handleDoubleClick,
  name,
  type,
  selectList,
}: Props) => {
  return (
    <div className={classes.select}>
      <h3>{name}:</h3>
      {editMode ? (
        <Select
          value={value}
          onChange={(e) => handleInput(e, name)}
          onKeyDown={(e) => handleKeyPress(e, name)}
        >
          {selectList?.map((e) => {
            return <Select.Option key={e}>{e}</Select.Option>
          })}
        </Select>
      ) : (
        createElement(
          type,
          { onDoubleClick: () => handleDoubleClick(name) },
          value
        )
      )}
    </div>
  )
}

export default TaskSelect
