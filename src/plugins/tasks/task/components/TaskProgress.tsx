import { createElement, KeyboardEvent } from 'react'
import { Slider } from 'antd'
import { Keys } from '@/plugins/tasks/task/utils'
import classes from '../task.module.scss'

type Props = {
  editMode: boolean
  value: number
  handleInput: (e: string | number | Date, name: Keys) => void
  handleKeyPress: (e: KeyboardEvent<HTMLElement>, name: Keys) => void
  handleDoubleClick: (name: Keys) => void
  name: Keys
  type: string
}
const TaskProgress = ({
  editMode,
  value,
  handleInput,
  handleKeyPress,
  handleDoubleClick,
  name,
  type,
}: Props) => {
  return (
    <div className={classes.progress}>
      <h3>{name}: </h3>
      {editMode ? (
        <div onKeyDown={(e) => handleKeyPress(e, name)}>
          <Slider value={value} onChange={(e) => handleInput(e, name)} />
        </div>
      ) : (
        createElement(
          type,
          { onDoubleClick: () => handleDoubleClick(name) },
          value + '%'
        )
      )}
    </div>
  )
}

export default TaskProgress
