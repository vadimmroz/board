import { createElement, KeyboardEvent } from 'react'
import { DatePicker } from 'antd'
import { Keys } from '@/plugins/tasks/task/utils'
import moment from 'moment'
import dayjs from 'dayjs'
import classes from '../task.module.scss'

type Props = {
  editMode: boolean
  value: Date
  handleInput: (e: string | number | Date, name: Keys) => void
  handleKeyPress: (e: KeyboardEvent<HTMLElement>, name: Keys) => void
  handleDoubleClick: (name: Keys) => void
  name: Keys
  type: string
  closeEditMode: (name: Keys) => void
}
const TaskDate = ({
  editMode,
  value,
  handleInput,
  handleKeyPress,
  handleDoubleClick,
  name,
  type,
  closeEditMode,
}: Props) => {
  return (
    <div className={classes.date}>
      <h3>{name}: </h3>
      {editMode ? (
        <div onKeyDown={(e) => handleKeyPress(e, name)}>
          <DatePicker
            onOk={() => closeEditMode(name)}
            onChange={(e) => handleInput(new Date(Number(e)), name)}
            showTime
            defaultValue={dayjs(value)}
          />
        </div>
      ) : (
        createElement(
          type,
          { onDoubleClick: () => handleDoubleClick(name) },
          moment(value).format('DD.MM.YYYY HH:mm')
        )
      )}
    </div>
  )
}

export default TaskDate
