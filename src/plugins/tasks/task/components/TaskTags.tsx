import { ChangeEvent, createElement, KeyboardEvent, useState } from 'react'
import { Input, Typography } from 'antd'
import { Keys } from '@/plugins/tasks/task/utils'
import classes from '../task.module.scss'

type Props = {
  editMode: boolean
  value: string[]
  handleInput: (e: string | number | Date | string[], name: Keys) => void
  handleKeyPress: (e: KeyboardEvent<HTMLElement>, name: Keys) => void
  handleDoubleClick: (name: Keys) => void
  name: Keys
  type: string
  closeEditMode: (name: Keys) => void
}
const TaskTags = ({
  editMode,
  value,
  handleInput,
  handleKeyPress,
  handleDoubleClick,
  name,
  type,
}: Props) => {
  const handleDeleteTag = (e: string) => {
    handleInput(
      value.filter((t) => t !== e),
      name
    )
  }
  const [tag, setTag] = useState('')
  const handleTag = (e: ChangeEvent<HTMLInputElement>) => {
    setTag(e.target.value)
  }
  const handleAddTag = () => {
    handleInput([...value, tag], name)
    setTag('')
  }

  return (
    <div className={classes.tagsBlock}>
      <h3>{name}: </h3>
      {editMode ? (
        <div
          onKeyDown={(e) => handleKeyPress(e, name)}
          className={classes.tags}
        >
          <div className={classes.input}>
            <Input onChange={handleTag} value={tag} />
            <button onClick={handleAddTag}>+</button>
          </div>
          <div className={classes.tagList}>
            {value.map((e) => {
              return (
                <div key={e}>
                  <Typography>#{e}</Typography>
                  <button onClick={() => handleDeleteTag(e)}>&times;</button>
                </div>
              )
            })}
          </div>
        </div>
      ) : (
        createElement(
          type,
          { onDoubleClick: () => handleDoubleClick(name) },
          value.map((e) => {
            return (
              <article key={e}>
                <Typography>#{e}</Typography>
              </article>
            )
          })
        )
      )}
    </div>
  )
}

export default TaskTags
