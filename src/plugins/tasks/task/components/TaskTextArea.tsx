import { createElement, KeyboardEvent } from 'react'
import classes from '../task.module.scss'
import { Keys } from '@/plugins/tasks/task/utils'
import TextArea from 'antd/es/input/TextArea'

type Props = {
  editMode: boolean
  value: string
  handleInput: (e: string | number | Date, name: Keys) => void
  handleKeyPress: (e: KeyboardEvent<HTMLElement>, name: Keys) => void
  handleDoubleClick: (name: Keys) => void
  name: Keys
  type: string
}
const TaskTextArea = ({
  editMode,
  value,
  handleInput,
  handleKeyPress,
  handleDoubleClick,
  name,
  type,
}: Props) => {
  return (
    <div className={classes.description}>
      <h3>{name}:</h3>
      {editMode ? (
        <TextArea
          value={value}
          onChange={(e) => handleInput(e.target.value, name)}
          onKeyDown={(e) => handleKeyPress(e, name)}
        />
      ) : (
        createElement(
          type,
          { onDoubleClick: () => handleDoubleClick(name) },
          value
        )
      )}
    </div>
  )
}

export default TaskTextArea
