import classes from './addCard.module.scss'
import ITask from '@/plugins/tasks/list/card/ITask'
import { ChangeEvent, Dispatch, KeyboardEvent, useState } from 'react'
import axios from 'axios'
import { url } from '@/utils/utils'
import { Button, DatePicker, Input, Select, Slider, Typography } from 'antd'
import TextArea from 'antd/es/input/TextArea'

type Props = {
  handleShowAddCardModal: () => void
  status: string
  cardList: ITask[]
  setCardList: Dispatch<ITask[]>
}

const AddCard = ({
  handleShowAddCardModal,
  status,
  cardList,
  setCardList,
}: Props) => {
  const [task, setTask] = useState<ITask>({
    name: 'Some name',
    author: 'Erik',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum illo nam neque nesciunt nulla officia.',
    img: 'string',
    progress: 20,
    owners: ['Erik'],
    moders: ['string'],
    guests: ['string'],
    priority: 'string',
    status: status,
    tags: ['In Progress', 'Date', 'Card'],
    date: new Date(),
    deadline: new Date(Number(new Date()) + 36000000),
    index: 1,
  })
  const [tag, setTag] = useState('')
  const handleAddCard = async () => {
    await axios.post(`${url}/todo-task`, task).then((res) => {
      setCardList([...cardList, res.data])
      handleShowAddCardModal()
    })
  }
  const handleInput = (e: string | number | Date, name: string) => {
    setTask({ ...task, [name]: e })
  }
  const handleTag = (e: ChangeEvent<HTMLInputElement>) => {
    setTag(e.target.value)
  }
  const handleAddTag = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      const tags = [...task.tags, tag]
      setTask({ ...task, tags: tags })
      setTag('')
      e.preventDefault()
      e.stopPropagation()
    }
  }

  const handleDeleteTag = (e: string) => {
    setTask({ ...task, tags: task.tags.filter((t) => t !== e) })
  }
  return (
    <article className={classes.addCard} onClick={handleShowAddCardModal}>
      <form
        onClick={(e) => {
          e.preventDefault()
          e.stopPropagation()
        }}
      >
        <button className={classes.close} onClick={handleShowAddCardModal}>
          &times;
        </button>
        <div>
          <Typography>Name:</Typography>
          <Input
            onChange={(e) => handleInput(e.target.value, 'name')}
            value={task.name}
            placeholder="name"
          />
        </div>
        <div>
          <Typography>Description: </Typography>
          <TextArea
            onChange={(e) => handleInput(e.target.value, 'description')}
            value={task.description}
            placeholder="description"
          />
        </div>
        <div>
          <Typography>Image:</Typography>
          <Input
            onChange={(e) => handleInput(e.target.value, 'img')}
            value={task.img}
            placeholder="img"
          />
        </div>
        <div>
          <Typography>Progress:</Typography>
          <Slider onChange={(e) => handleInput(e, 'progress')} />
        </div>
        <div>
          <Typography>Priority: </Typography>
          <Select
            placeholder="priority"
            onChange={(e) => handleInput(e, 'priority')}
          >
            <Select.Option key="low">Low</Select.Option>
            <Select.Option key="medium">Medium</Select.Option>
            <Select.Option key="high">High</Select.Option>
          </Select>
        </div>
        <div>
          <Typography>Tags</Typography>
          <Input onChange={handleTag} value={tag} onKeyPress={handleAddTag} />
          <div className={classes.tagList}>
            {task.tags.map((e) => {
              return (
                <div key={e}>
                  <Typography>#{e}</Typography>
                  <button onClick={() => handleDeleteTag(e)}>&times;</button>
                </div>
              )
            })}
          </div>
        </div>
        <div>
          <Typography>Deadline: </Typography>
          <DatePicker
            showTime
            onChange={(e) => {
              setTask({ ...task, deadline: new Date(Number(e)) })
            }}
          />
        </div>
        <Button onClick={handleAddCard}>Add Task</Button>
      </form>
    </article>
  )
}

export default AddCard
