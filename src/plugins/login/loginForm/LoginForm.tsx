'use client'
import classes from './loginForm.module.scss'
import { ChangeEvent, useState } from 'react'
import Button from '@/components/UIKit/button'
import axios from 'axios'
import { useRouter } from 'next/navigation'
import { url } from '@/utils/utils'

const LoginForm = () => {
  const [user, setUser] = useState({
    name: '',
    password: '',
  })
  const handleInput = (e: ChangeEvent<HTMLInputElement>, name: string) => {
    setUser({ ...user, [name]: e.target.value })
  }
  const router = useRouter()
  const handleLogin = () => {
    if (user.name.length > 0 && user.password.length > 0) {
      axios
        .post(`${url}/auth/login`, {
          name: user.name,
          password: user.password,
        })
        .then((res) => {
          if (res.data) {
            localStorage.setItem('token', res.data.access_token)
            router.push('/dashboard')
          }
        })
        .catch((err) => {
          console.log(err)
        })
    }
  }
  return (
    <section className={classes.loginForm}>
      <h3>Login</h3>
      <input
        value={user.name}
        onChange={(e) => handleInput(e, 'name')}
        placeholder="Username"
      />
      <input
        value={user.password}
        onChange={(e) => handleInput(e, 'password')}
        placeholder="Password"
      />
      <Button size="medium" color="default" onClick={handleLogin}>
        Login
      </Button>
    </section>
  )
}

export default LoginForm
