import { Event } from 'react-big-calendar'
import classes from './addEvent.module.scss'
import { DatePicker, Button, Input } from 'antd'
import { ChangeEvent, useState } from 'react'
import dayjs from 'dayjs'
import { defaultValue } from './utils'

const { RangePicker } = DatePicker
type TEvent = {
  eventList: Event[]
  handler: (eventList: Event[]) => void
  handleClose: () => void
}

const AddEvent = ({ eventList, handler, handleClose }: TEvent) => {
  const [event, setEvent] = useState<Event>(defaultValue)

  const handleSubmit = () => {
    handler([...eventList, { ...event, resource: Math.random() * 1000 }])
    handleClose()
  }
  const handleTitle = (e: ChangeEvent<HTMLInputElement>) => {
    setEvent({ ...event, title: e.target.value })
  }
  const handlePickDate = (dateStr: string[]) => {
    setEvent({
      ...event,
      start: new Date(dateStr[0]),
      end: new Date(dateStr[1]),
    })
  }

  return (
    <div className={classes.modal} onClick={handleClose}>
      <form
        className={classes.addEvent}
        onClick={(e) => {
          e.preventDefault()
          e.stopPropagation()
        }}
      >
        <button className={classes.close} onClick={handleClose}>
          &times;
        </button>
        <h5>Add Event</h5>
        <Input placeholder="Title" onChange={handleTitle} />
        <RangePicker
          defaultValue={[dayjs(), dayjs().add(3600000)]}
          showTime
          onChange={(_, dateString) => handlePickDate(dateString)}
        />
        <Button onClick={handleSubmit}>Add</Button>
      </form>
    </div>
  )
}

export default AddEvent
