'use client'
import { useEffect, useRef, useState } from 'react'
import { Calendar, Event, View } from 'react-big-calendar'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import {
  changeEventTime,
  defaultValues,
  EventFunc,
  localizer,
} from '@/plugins/calendar/myCalendar/utils'
import PageHeader from '@/components/pageHeader'
import Button from '@/components/UIKit/button'
import classes from './myCalendar.module.scss'
import { Portal } from 'next/dist/client/portal'
import AddEvent from '@/plugins/calendar/myCalendar/components/addEvent'
import autoAnimate from '@formkit/auto-animate'

const MyCalendar = () => {
  const [events, setEvents] = useState<Event[]>(defaultValues)
  const [view, setView] = useState<View>('month')
  const [date, setDate] = useState(new Date())
  const [showModal, setShowModal] = useState(false)

  const handleView = (view: View) => {
    setView(view)
  }
  const handleNavigate = (e: Date) => {
    setDate(e)
  }

  const onEventDrop: EventFunc = (data) => {
    const { start, end, event } = data
    setEvents((prev) => changeEventTime(prev, event, start, end))
  }
  const changeEvents = (eventList: Event[]) => {
    setEvents(eventList)
  }

  const handleModal = () => {
    setShowModal(!showModal)
  }
  const eventPropGetter = () => {
    return {
      style: {
        background: 'var(--blue)',
      },
    }
  }
  const parent = useRef(null)

  useEffect(() => {
    parent.current &&
      autoAnimate(parent.current, {
        duration: 300,
        easing: 'ease-in-out',
        disrespectUserMotionPreference: true,
      })
  }, [parent])

  return (
    <div ref={parent}>
      <PageHeader>
        <h5>My Calendar</h5>
        <Button size="medium" color="default" onClick={handleModal}>
          + Add Event
        </Button>
      </PageHeader>
      <div className={classes.calendar}>
        <DnDCalendar
          view={view}
          events={events}
          localizer={localizer}
          onEventDrop={onEventDrop}
          onEventResize={onEventDrop}
          onView={handleView}
          onNavigate={handleNavigate}
          eventPropGetter={eventPropGetter}
          resizable
          date={date}
          style={{ height: 'calc(80vh - 70px)' }}
        />
      </div>
      {showModal && (
        <AddEvent
          eventList={events}
          handler={changeEvents}
          handleClose={handleModal}
        />
      )}
    </div>
  )
}

const DnDCalendar = withDragAndDrop(Calendar)
export default MyCalendar
