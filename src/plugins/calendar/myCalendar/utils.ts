import { momentLocalizer, Event, stringOrDate } from 'react-big-calendar'
import moment from 'moment/moment'
import { withDragAndDropProps } from 'react-big-calendar/lib/addons/dragAndDrop'

const start = new Date()
const end = new Date(Number(start) + 3600000)
const start1 = new Date(Number(start) + 360000000)
const end1 = new Date(Number(start1) + 3600000)
export const defaultValues = [
  {
    title: 'Learn cool stuff',
    start,
    end,
    resource: 1,
  },
  {
    title: '34567',
    start: start1,
    end: end1,
    resource: 2,
  },
]

export const localizer = momentLocalizer(moment)

export type EventFunc =
  | withDragAndDropProps['onEventDrop']
  | withDragAndDropProps['onEventResize']

export const changeEventTime = (
  prevState: Event[],
  event: Event,
  start: stringOrDate,
  end: stringOrDate
) =>
  prevState.map((e) => {
    return e.resource === event.resource
      ? { ...e, end: new Date(end), start: new Date(start) }
      : e
  })
