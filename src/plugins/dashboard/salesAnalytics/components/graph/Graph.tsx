'use client'
import classes from './graph.module.scss'
import { Chart } from 'react-chartjs-2'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  Tooltip,
  PointElement,
  LineElement,
  BarElement,
  Filler,
  LineController,
  BarController,
} from 'chart.js'
import Preloader from '@/components/preloader'

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  LineController,
  BarController,
  Tooltip,
  BarElement,
  Filler
)
const labels = [
  'Mo',
  '',
  'Tu',
  '',
  'We',
  '',
  'Th',
  '',
  'Fr',
  '',
  'St',
  '',
  'San',
]
const datasets = [
  {
    type: 'line' as const,
    label: 'Conversation Ratio',
    borderColor: '#4D515A',
    borderWidth: 4,
    fill: true,
    data: new Array(14).fill(0).map(() => Math.random() * 10),
  },
  {
    type: 'line' as const,
    label: 'Sales',
    borderColor: '#F7B84B',
    borderWidth: 4,
    fill: false,
    data: new Array(14).fill(0).map(() => Math.random() * 10),
  },
  {
    type: 'bar' as const,
    label: 'Income',
    backgroundColor: '#5D5FEF',
    data: new Array(14).fill(0).map(() => Math.random() * 10),
    borderColor: 'white',
    borderWidth: 2,
  },
]
const Graph = () => {
  return (
    <div className={classes.graph}>
      <Chart
        type="bar"
        data={{
          labels,
          datasets,
        }}
        fallbackContent={<Preloader />}
      />
    </div>
  )
}

export default Graph
