import classes from './salesAnalytics.module.scss'
import PageHeader from '@/components/pageHeader'
import Button from '@/components/UIKit/button'
import Image from 'next/image'
import { lazy, Suspense } from 'react'
import Preloader from '@/components/preloader'

const Graph = lazy(() => import('./components/graph'))
const SalesAnalytics = () => {
  return (
    <article className={classes.salesAnalytics}>
      <PageHeader>
        <h5>Sales Analytics</h5>
        <div className={classes.row}>
          <p>Sort By</p>
          <Button size="medium" color="iconic" disabled>
            Weekly{' '}
            <Image
              width={24}
              height={24}
              src="/img/Chevron_Down.svg"
              alt="arrow"
            />
          </Button>
        </div>
      </PageHeader>
      <PageHeader>
        <p className={classes.headerText}>
          <span className={classes.blue}>$1,760</span> Income
        </p>
        <p className={classes.headerText}>
          <span>345</span> Sales
        </p>
        <p className={classes.headerText}>
          <span>3,5%</span> Conversation Ratio
        </p>
      </PageHeader>
      <Suspense fallback={<h1>sdfsdf</h1>}>
        <Graph />
      </Suspense>
    </article>
  )
}

export default SalesAnalytics
