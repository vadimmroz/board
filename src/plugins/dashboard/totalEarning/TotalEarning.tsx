import Card from '@/plugins/dashboard/components/card'
import { useState } from 'react'
import { useRouter } from 'next/navigation'
import axios from 'axios'
import { url } from '@/utils/utils'

const TotalEarning = async () => {
  const data = await axios.get<{
    name: string
    value: number
    increase: number
  }>(`${url}/analytics/TOTAL EARNINGS`)
  return (
    <Card
      name={data.data.name}
      img="/img/dollar-circle.svg"
      viewText="View net earnings"
      viewLink={'/net-earnings'}
      price={data.data.value}
      interest={data.data.increase}
    />
  )
}

export default TotalEarning
