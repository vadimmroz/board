import classes from './informationByStores.module.scss'
import PageHeader from '@/components/pageHeader'
import Button from '@/components/UIKit/button'
import Image from 'next/image'

const InformationByStores = () => {
  return (
    <div className={classes.InformationByStores}>
      <PageHeader>
        <h5 className={classes.text}>Information by stores (30)</h5>
        <div className={classes.row}>
          <p>Sort By</p>{' '}
          <Button size="small" color="default" disabled>
            Prosress{' '}
            <Image
              src="/img/Chevron_Down.svg"
              alt="arrow"
              width={24}
              height={24}
            />
          </Button>
        </div>
      </PageHeader>
    </div>
  )
}

export default InformationByStores
