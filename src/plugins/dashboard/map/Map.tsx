import classes from './map.module.scss'
import MapSvg from './components/mapSvg'
import PageHeader from '@/components/pageHeader'
import Button from '@/components/UIKit/button'
import Image from 'next/image'

const Map = () => {
  return (
    <div className={classes.map}>
      <PageHeader>
        <h5>Implementation of the monthly plan</h5>
        <Button size="small" color="default" variant="outlined" bordered>
          <Image
            src="/img/Dowmload.svg"
            alt="Dowmload"
            width={24}
            height={24}
          />
        </Button>
      </PageHeader>
      <MapSvg />
    </div>
  )
}

export default Map
