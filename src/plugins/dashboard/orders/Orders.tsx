import Card from '@/plugins/dashboard/components/card'
import { useState } from 'react'
import { useRouter } from 'next/navigation'
import axios from 'axios'
import { url } from '@/utils/utils'

const Orders = async () => {
  const data = await axios.get<{
    name: string
    value: number
    increase: number
  }>(`${url}/analytics/ORDERS`)
  return (
    <Card
      viewLink="/orders"
      img="/img/bag-2.svg"
      name={data.data.name}
      count={data.data.value}
      viewText="View all orders"
      interest={data.data.increase}
    />
  )
}

export default Orders
