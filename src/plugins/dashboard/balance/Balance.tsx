import { useState } from 'react'
import Card from '@/plugins/dashboard/components/card'
import { useRouter } from 'next/navigation'
import axios from 'axios'
import { url } from '@/utils/utils'

const Balance = async () => {
  const data = await axios.get<{
    name: string
    value: number
    increase: number
  }>(`${url}/analytics/BALANCE`)
  return (
    <Card
      name={data.data.name}
      interest={data.data.increase}
      price={data.data.value}
      viewText="Withdraw money"
      viewLink="/withdraw-money"
      img="/img/empty-wallet.svg"
    />
  )
}

export default Balance
