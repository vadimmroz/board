export const priceOrCount = (price?: number, count?: number) => {
  return price
    ? `$${price / 1000 > 1 ? price / 1000 + 'k' : price}`
    : count
      ? count > 0
        ? `+${count}`
        : `${count}`
      : 0
}
