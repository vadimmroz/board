import classes from './card.module.scss'
import { priceOrCount } from '@/plugins/dashboard/components/card/utils'
import Link from 'next/link'
import Button from '@/components/UIKit/button'
import Image from 'next/image'
import { AppRouterInstance } from 'next/dist/shared/lib/app-router-context.shared-runtime'
import { useRouter } from 'next/navigation'

type Props = {
  name: string
  viewText: string
  viewLink: string
  img: string
  interest?: number
  comparison?: number
  count?: number
  price?: number
}

const Card = ({
  name,
  img,
  count,
  price,
  comparison,
  interest,
  viewLink,
  viewText,
}: Props) => {
  return (
    <article className={classes.card}>
      <div>
        <p>{name}</p>
        {interest ? (
          interest >= 0 ? (
            <p className={classes.green}>+{interest}%</p>
          ) : (
            <p className={classes.red}>{interest}%</p>
          )
        ) : comparison ? (
          comparison > 0 ? (
            <p className={classes.green}>+{comparison}</p>
          ) : (
            <p className={classes.red}>-{comparison}</p>
          )
        ) : (
          ''
        )}
      </div>
      <h4>{priceOrCount(price, count)}</h4>
      <div>
        <Link href={viewLink}>{viewText}</Link>
        <Button size="small" color="iconic">
          <Image src={img} alt={name} width={24} height={24} />
        </Button>
      </div>
    </article>
  )
}

export default Card
