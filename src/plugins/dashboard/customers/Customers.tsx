import Card from '@/plugins/dashboard/components/card'
import axios from 'axios'
import { url } from '@/utils/utils'

const Customers = async () => {
  const data = await axios.get<{
    name: string
    value: number
    increase: number
  }>(`${url}/analytics/CUSTOMERS`)
  return (
    <Card
      name={data.data.name}
      viewText="See details"
      viewLink="/customers"
      img="/img/user.svg"
      comparison={data.data.increase}
      count={data.data.value}
    />
  )
}

export default Customers
