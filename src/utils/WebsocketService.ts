import io, { Socket } from 'socket.io-client'

export class WebsocketService {
  static socket: null | Socket = null

  static createConnection() {
    this.socket = io('http://localhost:8080')
    this.socket.on('connect', () => {})
    this.socket.on('disconnect', () => {})
  }
}
